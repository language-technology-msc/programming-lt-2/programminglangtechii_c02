# wordnet is considered a corpus so it imported from nltk.corpus
from nltk.corpus import wordnet as wn


"""
How to import and use wordnet in NLTK.
Do some basic things 
"""

syns = wn.synsets("dog")
print('synsets:', syns)
# synsets: [Synset('dog.n.01'), Synset('frump.n.01'), Synset('dog.n.03'), Synset('cad.n.01'), Synset('frank.n.02'), Synset('pawl.n.01'), Synset('andiron.n.01'), Synset('chase.v.01')]

# Get the first one
dog = wn.synset('dog.n.01')
print('dog:', dog);
# Output: Synset('dog.n.01') ...
# ...
# A synset has an id that follow the following pattern 'word.pos.nn'
# word: the word
# pos: POS tag
# nn: number
print('definition:', dog.definition())
# Print examples
print('examples:', dog.examples())

"""
Use wordnet to find synonyms and antonyms.
Synonyms might be useful in our sentiment analysis 
example. For example we have a feature 'ambience'
which its value can be calculated by taking into account
its synonyms. 
"""
synonyms = []
antonyms = []
for syn in wn.synsets("ambience"):
    for l in syn.lemmas():
        synonyms.append(l.name())
        if l.antonyms():
            antonyms.append(l.antonyms()[0].name())

print('synonyms:', synonyms)
print('antonyms:', antonyms)

"""
Hypernerms
"""
pizza = wn.synsets('pizza')[0]
print("Synset name :  ", pizza.name())
print("Synset hypernyms :  ", pizza.hypernyms())

pasta = wn.synsets('pasta')[0]
print("Synset name :  ", pasta.name())
print("Synset hypernyms :  ", pasta.hypernyms())

"""
Pizza and pasta have the same hypernym.
[Synset('dish.n.02')]
This might be useful while creating an NLP system.

There are several similarity measures avaiable at NLTK.
Let's test Leacock Chodorow Similarity 
"""

print("pizza pasta: ", pizza.lch_similarity(pasta))
# pizza pasta:  2.538973871058276
print("pizza dog: ", pizza.lch_similarity(dog))
# pizza dog:  0.9985288301111273

print("pizza pasta: ", pizza.shortest_path_distance(pasta))
print("pizza dog: ", pizza.shortest_path_distance(dog))
# pizza pasta:  2
# pizza dog:  13