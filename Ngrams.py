import nltk
# this is for tokenization; see lecture 01
from nltk.tokenize import word_tokenize

# this is for counting
from nltk.collections import Counter

# Read the contents of a file
file = open('./data/review.txt', 'r')
content = file.read()
#print(content)

print('unigrams')
# Tokenize the contents
tokens = nltk.word_tokenize(content)
print(tokens)

print('bigrams')
# get bigrams
bigrams = list(nltk.bigrams(tokens))
print(bigrams)
# count
counts = Counter(bigrams)
print(counts)

print('trigrams')
# get trigrams
trigrams = list(nltk.trigrams(tokens))
print(trigrams)
# count
counts = Counter(trigrams)
print(counts)