# Import nltk
import nltk
# We will need pandas to read the data from the tsv file
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import RegexpTokenizer

# Toy data
#data = ['The quick brown fox.','Jumps over the lazy dog!']

# Real data
# Read from csv
docs=pd.read_csv('C:/Users/galanisd/Desktop/LangTech/02/sentiment-analysis-on-movie-reviews/train.tsv', sep='\t')
data=docs['Phrase']

# It will filter symbols and numbers
tokenizer = RegexpTokenizer(r'[a-zA-Z0-9]+')


# instantiate the vectorizer object
vectorizer = CountVectorizer(lowercase=True,stop_words='english',ngram_range = (1,2),tokenizer = tokenizer.tokenize, max_features=50)
# How the  vectorizer decides which vocabulary it will use for the feature vectors?
# more frequent features -> sorted by name

# how to pass a vocabulary
#voc_list = ["good", "nice", "bad"]
#vectorizer = CountVectorizer(lowercase=True,stop_words='english',ngram_range = (1,2),tokenizer = tokenizer.tokenize, max_features=50, vocabulary=voc_list)

# convert the documents into a matrix
text_counts= vectorizer.fit_transform(data)
tokens = vectorizer.get_feature_names()

# Print vocabulary
print("Vocabulary")
print(vectorizer.vocabulary_)
df_vect = pd.DataFrame(data = text_counts.toarray(),columns = tokens)
print("Vectors")
print(df_vect)