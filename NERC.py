import nltk
# we will need tokenization
from nltk.tokenize import word_tokenize
# we will need POS tagging
from nltk.tag import pos_tag

# Read the contents of a file
file = open('./data/ner.txt', 'r')
content = file.read()
print(content)

# tokenize and POS
words= word_tokenize(content)
postags=pos_tag(words)

# POS tags are needed for NE recognition
ner = nltk.ne_chunk(postags,binary=False)
print(ner)
# Let's do some painting
ner.draw()


