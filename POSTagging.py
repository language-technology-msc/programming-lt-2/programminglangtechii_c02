# import nltk
import nltk

# this is for tokenization; see lecture 01
from nltk.tokenize import word_tokenize

# this is for importing the default pos tagger of nltk
from nltk.tag import pos_tag
# The default  pos tagger is PerceptronTagger
# Before using it you have to nltk.download('averaged_perceptron_tagger’)

# this is for counting
from nltk.collections import Counter

# this is for calculating frequencies.
from nltk.probability import FreqDist

# Read the contents of a file
file = open('./data/review.txt', 'r')
content = file.read()
print(content)

# Tokenize the contents
tokens = nltk.word_tokenize(content)
print(tokens)

# Get POS tags
tags = nltk.pos_tag(tokens)
print(tags)

# Get a description for a tag
#nltk.help.upenn_tagset('RB')
# Before using it you  you have to nltk.download('tagsets')

# Is it possible to get UD POS tags (???)
# Yes NLTK offers a mapping to  a variant of UD tagset
# More info at https://www.nltk.org/_modules/nltk/tag/mapping.html
# Requires nltk.download('universal_tagset')
ud_tags = nltk.pos_tag(tokens, 'universal')
print(ud_tags)

# ----------------------------------------


# I am insterested only in
ud = {'ADV','ADJ'}
# Copy only these in a new array
just_pos = []
for tag in ud_tags:
  if tag[1] in ud:
    #print(tag[0])
    #print(tag[1])
    just_pos.append(tag[1])

# Calculate frequencies with FreqDist
fdist = FreqDist(just_pos)
fdist_most_common = fdist.most_common(2)
print(fdist_most_common)

# Calculate frequencies with Counter
counts = Counter(just_pos)
print(counts)




