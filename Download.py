import nltk
from bs4 import BeautifulSoup
from urllib.request import urlopen
from nltk.tokenize import word_tokenize
from nltk.collections import Counter

# extract all the contents of the text file.
raw = urlopen("https://www.nytimes.com/2021/03/02/opinion/misinformation-conspiracy-theories.html?action=click&module=Opinion&pgtype=Homepage").read()
#print('raw:', raw)

# remove html/xml tags
clean = BeautifulSoup(raw, features="html.parser")
# get text and print it
text= clean.get_text()
print('clean:', text)

# tokenize as usual
tokens = nltk.word_tokenize(text)
# count
counts = Counter(tokens)
print(counts)
